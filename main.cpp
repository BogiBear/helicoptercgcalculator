#include <iostream>
#include "PassengerData.h"


// needed variables
/*
 * Longitudinal Arm
 * Lateral Arm
 * Empty Lateral arm
 *
 * Longitudinal arm - Pilotweight * Longitudinal arm
 * Lateral arm Pilot - Pilotweight * Lateral Arm
 * Lateral arm Passenger - Passenger weight * Lateral arm (negative number)
 * Add Empty Weight + pilot + passengers
 * Add the moments as well for pilot + passenger and actual moment
 * Empty lateral moments need to be added as well.
 *
 * CG Longitudinal = Long moment / divide by empty weight
 * CG Lateral = Lateral moment / divide by empty weight
 *
 * Fuel added - Main tank
 * Fuel added - Aux tank
 *
 * Long Moment with fuel - Fuel * Longitudinal moment
 * Lat moment with fuel - Fuel * Lateral moment
 *
 * Long Total moment -  empty long moment + fuel moments
 * Lateral total moment - empty lat moment + fuel amounts
 * Total moment with fuel - Total long arm * total weight
 * Total lat moment with fuel - Total lat arm * weight
 */

using namespace std;
int main() {

    std::cout << "Creating a new helicopter with hardcoded data and returning appropriate calculations!" << std::endl;

    auto* helicopter = new PassengerData(103.6, -0.46, 883, 220, 150);
    helicopter->setPilotLongArm(78.0);
    helicopter->setPassengerLongArm(78.8);
    helicopter->setPilotLatArm(10.7);
    helicopter->setPassengerLatArm(-9.3);
    // below we calculate data with fuel in main and auxiliary fuel tank
    helicopter->setMainTankCapacity(11);
    helicopter->setAuxTankCapacity(6);
    helicopter->setMainTankLongArm(108.6);
    helicopter->setMainTankLatArm(-11.0);
    helicopter->setAuxTankLongArm(103.8);
    helicopter->setAuxTankLatArm(11.2);

    cout << "Helicopter longitudinal moment is: " << helicopter->calculateEmptyLongMoment() << endl;
    cout << "Helicopter lateral moment is: " << helicopter->calculateEmptyLatMoment() << endl;
    cout << "Pilot longitudinal moment is: " << helicopter->calculatePilotLongMoment() << endl;
    cout << "Pilot lateral moment is: " << helicopter->calculatePilotLatMoment() << endl;
    cout << "Pilot longitudinal moment is: " << helicopter->calculatePassengerLongMoment() << endl;
    cout << "Pilot lateral moment is: " << helicopter->calculatePassengerLatMoment() << endl;

    // calculations for total weight and fully added moments
    cout << "Total helicopter weight: " << helicopter->calculateFullWeight() << endl;
    cout << "Total longitudinal moment: "<< helicopter->calculateFullLongMoment() << endl;
    cout << "Total lateral moment: "  << helicopter->calculateFullLatMoment() << endl;

    cout << "Full Long arm is: " << helicopter->calculateFullLongArm() << endl;
    cout << "Full Lat Arm is: " << helicopter->calculateFullLatArm() << endl;

    cout << "Main tank fuel weight (lbs): " << helicopter->getMainTankCapacity() << endl;
    cout << "Auxiliary tank fuel weight (lbs): " << helicopter->getAuxTankCapacity() << endl;

    double takeOffWeight = helicopter->calculateFullWeight() +
                           helicopter->getMainTankCapacity() +
                           helicopter->getAuxTankCapacity();

    cout << "We are taking off with :" << takeOffWeight << "lbs of fuel" << endl;


    double mainLongMoment = helicopter->getMainTankCapacity() * helicopter->getMainTankLongArm();
    double mainLatMoment = helicopter->getMainTankCapacity() * helicopter->getMainTankLatArm();
    double auxLongMoment = helicopter->getAuxTankCapacity() * helicopter->getAuxTankLongArm();
    double auxLatMoment = helicopter->getAuxTankCapacity() * helicopter->getAuxTankLatArm();

    cout << "Helicopter main fuel longitudinal moment: " << mainLongMoment << endl;
    cout << "Helicopter main fuel lateral moment: " << mainLatMoment << endl;

    cout << "Helicopter aux fuel longitudinal moment: " << auxLongMoment << endl;
    cout << "Helicopter aux fuel lateral moment: " << auxLatMoment << endl;

    double totalLongMoment = helicopter->calculateFullLongMoment() + mainLongMoment + auxLongMoment;
    double totalLatMoment = helicopter->calculateFullLatMoment() + mainLatMoment + auxLatMoment;

    double takeOffLongArm = totalLongMoment / takeOffWeight;
    double takeOffLatArm = totalLatMoment / takeOffWeight;

    cout << "We are taking off weight a longitudinal CG of: " << takeOffLongArm << " and lateral CG of " << takeOffLatArm << endl;

    return 0;
}