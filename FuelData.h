//
// Created by Bogdan Tudosie on 05/09/2019.
//

#ifndef CG_CALCULATOR_FUELDATA_H
#define CG_CALCULATOR_FUELDATA_H


class FuelData {


public:
    FuelData(),
            FuelData(double mainTankLA, double mainTankLatArm, double auxTankLA, double auxTankLatArm,
                     double mainTankCap, double auxTankCap);
    double getMainTankLongArm() const;
    void setMainTankLongArm(double mainTankLongArm);

    double getAuxTankLongArm() const;
    void setAuxTankLongArm(double auxTankLongArm);

    double getAuxTankLatArm() const;
    void setAuxTankLatArm(double auxTankLatArm);

    double getMainTankCapacity() const;
    void setMainTankCapacity(double mainTankCapacity);

    double getAuxTankCapacity() const;
    void setAuxTankCapacity(double auxTankCapacity);

private:
    // fuel longitudinal and lateral arms for fuel tanks
    double mainTankLongArm{};
    double mainTankLatArm{};
    double auxTankLongArm{};
    double auxTankLatArm{};

    // Fuel data
    double mainTankCapacity{};
    double auxTankCapacity{};
};


#endif //CG_CALCULATOR_FUELDATA_H
