//
// Created by Bogdan Tudosie on 05/09/2019.
//

#include "FuelData.h"

FuelData::FuelData() = default;

FuelData::FuelData(double mainTankLA, double mainTankLatArm, double auxTankLA, double auxTankLatArm, double mainTankCap,
                   double auxTankCap):mainTankLongArm(mainTankLA), mainTankLatArm(mainTankLatArm),
                   auxTankLongArm(auxTankLA), auxTankLatArm(auxTankLatArm),mainTankCapacity(mainTankCap),
                   auxTankCapacity(auxTankCap) {

}

double FuelData::getMainTankLongArm() const {
    return mainTankLongArm;
}

void FuelData::setMainTankLongArm(double mainTankLongArm) {
    FuelData::mainTankLongArm = mainTankLongArm;
}

double FuelData::getAuxTankLongArm() const {
    return auxTankLongArm;
}

void FuelData::setAuxTankLongArm(double auxTankLongArm) {
    FuelData::auxTankLongArm = auxTankLongArm;
}

double FuelData::getAuxTankLatArm() const {
    return auxTankLatArm;
}

void FuelData::setAuxTankLatArm(double auxTankLatArm) {
    FuelData::auxTankLatArm = auxTankLatArm;
}

double FuelData::getMainTankCapacity() const {
    return mainTankCapacity;
}

void FuelData::setMainTankCapacity(double mainTankCapacity) {
    FuelData::mainTankCapacity = mainTankCapacity * 8.35;
}

double FuelData::getAuxTankCapacity() const {
    return auxTankCapacity;
}

void FuelData::setAuxTankCapacity(double auxTankCapacity) {
    FuelData::auxTankCapacity = auxTankCapacity * 8.35;
}
