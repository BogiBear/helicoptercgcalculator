//
// Created by Bogdan Tudosie on 2019-06-27.
//

#include "PassengerData.h"

PassengerData::PassengerData() = default;

PassengerData::PassengerData(double longitudinalArm, double lateralArm, double weightInPounds, double pilotWeight,
                             double passengerWeight) : longitudinalArm(longitudinalArm), lateralArm(lateralArm),
                                                         weightInPounds(weightInPounds), pilotWeight(pilotWeight),
                                                         passengerWeight(passengerWeight) {}

void PassengerData::setPilotLongArm(double pilotLongArm) {
    PassengerData::pilotLongArm = pilotLongArm;
}

void PassengerData::setPilotLatArm(double pilotLatArm) {
    PassengerData::pilotLatArm = pilotLatArm;
}

void PassengerData::setPassengerLongArm(double passengerLongArm) {
    PassengerData::passengerLongArm = passengerLongArm;
}

void PassengerData::setPassengerLatArm(double passengerLatArm) {
    PassengerData::passengerLatArm = passengerLatArm;
}

// Calculation functions

double PassengerData::calculateEmptyLongMoment() {
    return weightInPounds * longitudinalArm;
}

double PassengerData::calculateEmptyLatMoment() {
    return weightInPounds * lateralArm;
}

double PassengerData::calculatePilotLongMoment() {
    return pilotWeight * pilotLongArm;
}

double PassengerData::calculatePilotLatMoment() {
    return pilotWeight * pilotLatArm;
}

double PassengerData::calculatePassengerLongMoment() {
    return passengerWeight * passengerLongArm;
}

double PassengerData::calculatePassengerLatMoment() {
    return passengerWeight * passengerLatArm;
}

double PassengerData::calculateFullWeight() {
    return weightInPounds + pilotWeight + passengerWeight;
}

double PassengerData::calculateFullLongMoment() {
    return this->calculateEmptyLongMoment() + this->calculatePilotLongMoment() + this->calculatePassengerLongMoment();
}

double PassengerData::calculateFullLatMoment() {
    return this->calculatePilotLatMoment() + this->calculateEmptyLatMoment() + this->calculatePassengerLatMoment();
}

double PassengerData::calculateFullLongArm() {
    return this->calculateFullLongMoment() / this->calculateFullWeight();
}

double PassengerData::calculateFullLatArm() {
    return this->calculateFullLatMoment() / this->calculateFullWeight();
}

