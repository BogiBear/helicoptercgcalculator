//
// Created by Bogdan Tudosie on 05/09/2019.
//

#ifndef CG_CALCULATOR_HELICOPTER_H
#define CG_CALCULATOR_HELICOPTER_H


#include <string>
#include "PassengerData.h"
#include "FuelData.h"
#include "CargoData.h"

using namespace std;

// class used to store a default helicopter based on a JSON Representation

class Helicopter {
public:
    Helicopter();
    Helicopter(std::string model, std::string manufacturer, double weight, PassengerData passengerData,
               FuelData fuelData, CargoData cargoData);
private:
    std::string model;
    std::string manufacturer;
    double weight;
    PassengerData passengerData;
    FuelData fuelData;
    CargoData cargoData;
protected:
};


#endif //CG_CALCULATOR_HELICOPTER_H
