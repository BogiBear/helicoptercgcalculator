//
// Created by Bogdan Tudosie on 2019-06-27.
//

#ifndef CG_CALCULATOR_PASSENGERDATA_H
#define CG_CALCULATOR_PASSENGERDATA_H


class PassengerData {
public:
    PassengerData();

    PassengerData(double longitudinalArm, double lateralArm, double weightInPounds, double pilotWeight,
                  double passengerWeight);

    void setPilotLongArm(double pilotLongArm);

    void setPilotLatArm(double pilotLatArm);

    void setPassengerLongArm(double passengerLongArm);

    void setPassengerLatArm(double passengerLatArm);

    double getAuxTankLongArm() const;
    void setAuxTankLongArm(double auxTankLongArm);
    double getAuxTankLatArm() const;
    void setAuxTankLatArm(double auxTankLatArm);

    double getMainTankLongArm() const;
    void setMainTankLongArm(double mainTankLongArm);
    double getMainTankLatArm() const;
    void setMainTankLatArm(double mainTankLatArm);

    double getMainTankCapacity() const;
    void setMainTankCapacity(double mainTankCapacity);
    double getAuxTankCapacity() const;
    void setAuxTankCapacity(double auxTankCapacity);


    double calculateEmptyLongMoment();
    double calculateEmptyLatMoment();
    double calculatePilotLongMoment();
    double calculatePilotLatMoment();
    double calculatePassengerLongMoment();
    double calculatePassengerLatMoment();
    double calculateFullWeight();
    double calculateFullLongMoment();
    double calculateFullLatMoment();
    double calculateFullLongArm();
    double calculateFullLatArm();

private:
    double longitudinalArm;
    double lateralArm;
    double pilotLongArm;
    double pilotLatArm;
    double passengerLongArm;
    double passengerLatArm;
    double weightInPounds;
    double pilotWeight;
    double passengerWeight;
};


#endif //CG_CALCULATOR_PASSENGERDATA_H
